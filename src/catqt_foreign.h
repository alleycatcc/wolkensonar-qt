#ifndef CATQT_FOREIGN_H
#define CATQT_FOREIGN_H

#define CATQT_FOREIGN_ROOT CatWolkRoot

#include "catqt_main.h"

#include "cat_wolk_root.h"

#ifdef PLATFORM_ANDROID

// --- This is a static structure in a header file, because getting it
// through a method called on the 'foreign root' is tricky (it might not exist yet when
// `JNI_OnLoad` is called).

catqt_jniSpec catqt_foreign_jniSpec[] = {};

#endif

#endif
