package cc.alleycat.android.wolkensonar;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;

import android.hardware.camera2.*;
import android.hardware.camera2.CameraManager.TorchCallback;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.R.id;
import android.os.Looper;
import android.os.Handler;

import org.qtproject.qt5.android.bindings.QtActivity;
import org.qtproject.qt5.android.QtLayout;
import org.qtproject.qt5.android.QtSurface;

import cc.alleycat.android.qtwebview.QtWebViewCommon;

public class JNIActivity extends QtActivity {
    private static final String TAG = "Wolkensonar.JNIActivity";
    private static final String HOST_CHANNEL_FROM_JS = "hostChannelFromJS";

    private static JNIActivity instance;

    private static boolean initted = false;

    private static CameraManager cameraManager;
    private static String cameraId;

    private static WebView webview;

    public JNIActivity() {
        instance = this;
    }

    public static boolean init() {
        Log.w(TAG, String.format("Beginning init"));

        webview = getWebView();
        if (webview == null) return errorf("init: can't get webview");

        initJSInterface(webview);

        Log.w(TAG, String.format("init successful"));
        initted = true;

        return true;
    }

    public static boolean pageLoaded() {
        if (!initted) return errorf("pageLoaded: not initted");
        QtWebViewCommon.pageLoaded(instance, webview);
        return true;
    }

    private static WebView getWebView() {
        View child;
        int childCount;
        View rootview = instance.findViewById(android.R.id.content);
        Log.w(TAG, String.format("root: %s", rootview.toString()));

        try {
            FrameLayout rootlayout = (FrameLayout)rootview;
            Log.w(TAG, String.format("root framelayout: %s", rootlayout.toString()));
            childCount = rootlayout.getChildCount();
            Log.w(TAG, String.format("num children of frameview: %d", childCount));
            child = rootlayout.getChildAt(0);
            Log.w(TAG, String.format("child frameview: %s", child.toString()));
            QtLayout qtlayout = (QtLayout)child;
            Log.w(TAG, String.format("qtlayout: %s", qtlayout.toString()));
            childCount = qtlayout.getChildCount();
            Log.w(TAG, String.format("num children of qtlayout: %d", childCount));

            child = qtlayout.getChildAt(0);
            Log.w(TAG, String.format("qtlayout child 0: %s", child.toString()));
            QtSurface qtsurface = (QtSurface)child;

            // --- note, don't try to get children of qtsurface: it's a view.
            Log.w(TAG, String.format("qtsurface: %s", qtsurface.toString()));

            child = qtlayout.getChildAt(1);
            Log.w(TAG, String.format("qtlayout child 1: %s", child.toString()));
        } catch (RuntimeException e) {
            Log.w(TAG, String.format("layout not as expected, can't find webview"));
            return null;
        }

        return (WebView)child;
    }

    private static void initJSInterface(final WebView webview) {
        instance.runOnUiThread(new Runnable() {
            public void run() {
                webview.addJavascriptInterface(new JsInterface(), HOST_CHANNEL_FROM_JS);
            }
        });
    }

    private static boolean errorf(String err) {
        error(err);
        return false;
    }

    private static void error(String err) {
        Log.e(TAG, String.format("error: %s", err));
    }

    static class JsInterface {
    }
}
