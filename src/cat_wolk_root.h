#ifndef CAT_TOP_H
#define CAT_TOP_H

#include <vector>

#ifdef PLATFORM_ANDROID
# include <android/log.h>
# include <jni.h>
# include <QtAndroidExtras/QAndroidJniEnvironment>
# include <QtAndroidExtras/QAndroidJniObject>
#endif

#include <QQmlApplicationEngine>

#include "catqt_global.h"
#include "catqt_qml.h"

using std::vector;

class CatWolkRoot : public QObject
{
    Q_OBJECT

public:
    explicit CatWolkRoot(QObject *parent = nullptr);

    static CatWolkRoot *instance;
    void setQmlApplicationEngine(QQmlApplicationEngine *);

// --- these should be slots in order to be called from qml.
public slots:
    bool initActivity();
    bool pageLoaded();

private:
    QQmlApplicationEngine *engine;
#ifdef PLATFORM_ANDROID
    bool initActivityAndroid();
    bool pageLoadedAndroid();
#endif

protected:
};

#endif
