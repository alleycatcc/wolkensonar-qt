#include <iostream>

#include "cat_wolk_root.h"

CatWolkRoot *CatWolkRoot::instance = nullptr;

CatWolkRoot::CatWolkRoot(QObject *parent) : QObject(parent)
{
    instance = this;
}

void CatWolkRoot::setQmlApplicationEngine(QQmlApplicationEngine *engine)
{
    this->engine = engine;
}

bool CatWolkRoot::initActivity()
{
#ifdef PLATFORM_ANDROID
    return initActivityAndroid();
#else
    std::cout << "[mock] initActivity" << std::endl;
    return true;
#endif
}

bool CatWolkRoot::pageLoaded()
{
#ifdef PLATFORM_ANDROID
    return pageLoadedAndroid();
#else
    std::cout << "[mock] pageLoaded" << std::endl;
    return true;
#endif
}

#ifdef PLATFORM_ANDROID
bool CatWolkRoot::initActivityAndroid()
{
    const char *signature = "()Z";
    jboolean ok = QAndroidJniObject::callStaticMethod<jboolean>(
        ANDROID_NOTIFICATION_CLASS,
        "init",
        signature
    );
    return (bool)ok;
}
#endif

#ifdef PLATFORM_ANDROID
bool CatWolkRoot::pageLoadedAndroid()
{
    const char *signature = "()Z";
    jboolean ok = QAndroidJniObject::callStaticMethod<jboolean>(
        ANDROID_NOTIFICATION_CLASS,
        "pageLoaded",
        signature
    );
    return (bool)ok;
}
#endif

/*
void CatWolkRoot::quit()
{
    std::cout << "CatWolkRoot::quit" << std::endl;
    QGuiApplication::instance()->quit();
}

// --- open a link using a desktop service.
void CatWolkRoot::openLink(QString url)
{
    std::cout << "CatWolkRoot::openLink" << std::endl;
    (void)url;
    // QDesktopServices::openUrl(QUrl(url));
}
*/

