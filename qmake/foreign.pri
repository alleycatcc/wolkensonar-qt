HEADERS += catqt_foreign.h cat_wolk_root.h
SOURCES +=                 cat_wolk_root.cpp

android: {
    DEFINES += ANDROID_NOTIFICATION_CLASS=\\\"cc/alleycat/android/wolkensonar/JNIActivity\\\"
    OTHER_FILES += android-sources/src/cc/alleycat/android/wolkensonar/JNIActivity.java
}
